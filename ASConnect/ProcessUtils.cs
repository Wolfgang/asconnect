﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Security.Principal;

//PInvoke CreateProcessAsUser
//The following code demonstrates how to PInvoke CreateProcessAsUser
//without the logon credentials.
//This code works best when run as an Admin user.


namespace ASConnect
{

    [StructLayout(LayoutKind.Sequential)]
    internal struct PROCESS_INFORMATION
    {
        public IntPtr hProcess;
        public IntPtr hThread;
        public uint dwProcessId;
        public uint dwThreadId;
    }



    [StructLayout(LayoutKind.Sequential)]
    internal struct SECURITY_ATTRIBUTES
    {
        public uint nLength;
        public IntPtr lpSecurityDescriptor;
        public bool bInheritHandle;
    }


    [StructLayout(LayoutKind.Sequential)]
    internal struct STARTUPINFO
    {
        public uint cb;
        public string lpReserved;
        public string lpDesktop;
        public string lpTitle;
        public uint dwX;
        public uint dwY;
        public uint dwXSize;
        public uint dwYSize;
        public uint dwXCountChars;
        public uint dwYCountChars;
        public uint dwFillAttribute;
        public uint dwFlags;
        public short wShowWindow;
        public short cbReserved2;
        public IntPtr lpReserved2;
        public IntPtr hStdInput;
        public IntPtr hStdOutput;
        public IntPtr hStdError;

    }

    internal enum SECURITY_IMPERSONATION_LEVEL
    {
        SecurityAnonymous,
        SecurityIdentification,
        SecurityImpersonation,
        SecurityDelegation
    }

    internal enum TOKEN_TYPE
    {
        TokenPrimary = 1,
        TokenImpersonation
    }

    internal class ProcessAsUser
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(ProcessAsUser));
        public static string _retString;

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool CreateProcessAsUser(
            IntPtr hToken,
            string lpApplicationName,
            string lpCommandLine,
            ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation);


        [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx", SetLastError = true)]
        private static extern bool DuplicateTokenEx(
            IntPtr hExistingToken,
            uint dwDesiredAccess,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            Int32 ImpersonationLevel,
            Int32 dwTokenType,
            ref IntPtr phNewToken);


        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool OpenProcessToken(
            IntPtr ProcessHandle,
            UInt32 DesiredAccess,
            ref IntPtr TokenHandle);

        [DllImport("userenv.dll", SetLastError = true)]
        private static extern bool CreateEnvironmentBlock(
                ref IntPtr lpEnvironment,
                IntPtr hToken,
                bool bInherit);


        [DllImport("userenv.dll", SetLastError = true)]
        private static extern bool DestroyEnvironmentBlock(
                IntPtr lpEnvironment);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool CloseHandle(
            IntPtr hObject);

        private const short SW_SHOW = 5;
        private const uint TOKEN_QUERY = 0x0008;
        private const uint TOKEN_DUPLICATE = 0x0002;
        private const uint TOKEN_ASSIGN_PRIMARY = 0x0001;
        private const int GENERIC_ALL_ACCESS = 0x10000000;
        private const int STARTF_USESHOWWINDOW = 0x00000001;
        private const int STARTF_FORCEONFEEDBACK = 0x00000040;
        private const uint CREATE_UNICODE_ENVIRONMENT = 0x00000400;


        private static bool LaunchProcessAsUser(string cmdLine, IntPtr token, IntPtr envBlock)
        {
            bool result = false;


            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            SECURITY_ATTRIBUTES saProcess = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES saThread = new SECURITY_ATTRIBUTES();
            saProcess.nLength = (uint)Marshal.SizeOf(saProcess);
            saThread.nLength = (uint)Marshal.SizeOf(saThread);

            STARTUPINFO si = new STARTUPINFO();
            si.cb = (uint)Marshal.SizeOf(si);

            //if this member is NULL, the new process inherits the desktop 
            //and window station of its parent process. If this member is 
            //an empty string, the process does not inherit the desktop and 
            //window station of its parent process; instead, the system 
            //determines if a new desktop and window station need to be created. 
            //If the impersonated user already has a desktop, the system uses the 
            //existing desktop.

            si.lpDesktop = @"WinSta0\Default"; //Modify as needed
            si.dwFlags = STARTF_USESHOWWINDOW | STARTF_FORCEONFEEDBACK;
            si.wShowWindow = SW_SHOW;
            //Set other si properties as required.

            result = CreateProcessAsUser(
                token,
                null,
                cmdLine,
                ref saProcess,
                ref saThread,
                false,
                CREATE_UNICODE_ENVIRONMENT,
                envBlock,
                null,
                ref si,
                out pi);
            _retString = si.hStdOutput.ToString();

            if (result == false)
            {
                int error = Marshal.GetLastWin32Error();
                //log.Error(String.Format("CreateProcessAsUser Error: {0}", error));
            }

            return result;
        }


        private static IntPtr GetPrimaryToken(int processId)
        {
            IntPtr token = IntPtr.Zero;
            IntPtr primaryToken = IntPtr.Zero;
            bool retVal = false;
            Process p = null;

            try
            {
                p = Process.GetProcessById(processId);
            }

            catch (ArgumentException)
            {
                //log.Error(String.Format("ProcessID {0} Not Available", processId), ex);
                throw;
            }


            //Gets impersonation token
            retVal = OpenProcessToken(p.Handle, TOKEN_DUPLICATE, ref token);
            if (retVal == true)
            {

                SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();
                sa.nLength = (uint)Marshal.SizeOf(sa);

                //Convert the impersonation token into Primary token
                retVal = DuplicateTokenEx(
                    token,
                    TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_QUERY,
                    ref sa,
                    (int)SECURITY_IMPERSONATION_LEVEL.SecurityIdentification,
                    (int)TOKEN_TYPE.TokenPrimary,
                    ref primaryToken);

                //Close the Token that was previously opened.
                CloseHandle(token);
                if (retVal == false)
                {
                    //log.Error(String.Format("DuplicateTokenEx Error: {0}", Marshal.GetLastWin32Error()));
                }

            }
            else
            {
                //log.Error(String.Format("OpenProcessToken Error: {0}", Marshal.GetLastWin32Error()));
            }

            //We'll Close this token after it is used.
            return primaryToken;

        }

        private static IntPtr GetEnvironmentBlock(IntPtr token)
        {

            IntPtr envBlock = IntPtr.Zero;
            bool retVal = CreateEnvironmentBlock(ref envBlock, token, false);
            if (retVal == false)
            {
                //Environment Block, things like common paths to My Documents etc.
                //Will not be created if "false"
                //It should not adversley affect CreateProcessAsUser.
                //log.Error(String.Format("CreateEnvironmentBlock Error: {0}", Marshal.GetLastWin32Error()));
            }
            return envBlock;
        }

        /*
        private static Process FindProcessOfUser(string username, ref StringBuilder bld, Process[] processes)
        {
            foreach (Process process in processes)
            {
                try
                {
                    bld.Append(process.MainModule.ModuleName);
                    bld.Append(": ");
                }
                catch (Exception ex)
                {
                    bld.Append("[EXC ON DETERMINING MAIN MODULE NAME: " + ex.Message + "] ");
                    bld.Append(process.ToString());
                    bld.Append(": ");
                }
                try
                {
                    IntPtr token = GetPrimaryToken(process.Id);
                    if (token != IntPtr.Zero)
                    {
                        WindowsIdentity identity = new WindowsIdentity(token);
                        string nameCompare = identity.Name.Trim();
                        bld.Append(" Owner: ");
                        bld.Append(nameCompare);
                        bld.Append(" [Username to compare with is " + username + "]");
                        //if (nameCompare.Contains("\\"))
                        //{
                        //    nameCompare = nameCompare.Substring(nameCompare.LastIndexOf("\\") + 1);
                        //}
                        if (identity.Name.EndsWith(username))
                        {
                            bld.Append(" - USE THIS PROCESS");
                            return process;
                        }
                    }
                    else
                    {
                        //do nothing here - just means that no acces (sometimes Intptr.zero is returned, sometimes exception...)
                        bld.Append(" - TOKEN IS ZERO");
                    }
                }
                catch (Exception ex)
                {
                    //do nothing here - just means that no acces (sometimes Intptr.zero is returned, sometimes exception...)
                    bld.Append(" - EXC ON GETPRIMARYTOKEN: " + ex.Message);
                }
                bld.Append(Environment.NewLine);
            }
            return null;
        }
        */
        private static Process FindProcessOfUser(string username, ref StringBuilder bld, Process[] processes)
        {
            for (int i = 0; i < processes.Length; i++)
            {
                Process process = processes[i];
                try
                {
                    bld.Append(process.MainModule.ModuleName);
                    bld.Append(": ");
                }
                catch (Exception ex)
                {
                    bld.Append("[EXC ON DETERMINING MAIN MODULE NAME: " + ex.Message + "] ");
                    bld.Append(process.ToString());
                    bld.Append(": ");
                }
                try
                {
                    IntPtr primaryToken = ProcessAsUser.GetPrimaryToken(process.Id);
                    if (primaryToken != IntPtr.Zero)
                    {
                        WindowsIdentity windowsIdentity = new WindowsIdentity(primaryToken);
                        string value = windowsIdentity.Name.Trim();
                        bld.Append(" Owner: ");
                        bld.Append(value);
                        bld.Append(" [Username to compare with is " + username + "]");

                        if (windowsIdentity.Name.EndsWith(username, StringComparison.OrdinalIgnoreCase))
                        {
                            bld.Append(" - USE THIS PROCESS");
                            return process;
                        }
                    }
                    else
                    {
                        bld.Append(" - TOKEN IS ZERO");
                    }
                }
                catch (Exception ex2)
                {
                    bld.Append(" - EXC ON GETPRIMARYTOKEN: " + ex2.Message);
                }
                bld.Append(Environment.NewLine);

            }
            return null;
        }



        private static Process GetProcessOfUser(string username, ref StringBuilder bld)
        {
            return GetProcessOfUser(username, null, ref bld);
        }

        private static Process GetProcessOfUser(string username, string processname, ref StringBuilder bld)
        {
            username = username.Trim();
            bld = new StringBuilder();

            try
            {
                bld.Append("Search all processes:");
                bld.Append(Environment.NewLine);
                Process[] processesToCheck = null;
                if (string.IsNullOrEmpty(processname))
                {
                    bld.Append("Checked processes (Process.GetProcesses()):");
                    bld.Append(Environment.NewLine);
                    processesToCheck = Process.GetProcesses();
                }
                else
                {
                    bld.Append("Checked processes (Process.GetProcessesByName(" + processname + ")):");
                    bld.Append(Environment.NewLine);
                    processesToCheck = Process.GetProcessesByName(processname);
                }
                Process process = FindProcessOfUser(username, ref bld, processesToCheck);
                if (process != null)
                {
                    return process;
                }
            }
            catch (Exception ex)
            {
                bld.Append("Failed to search processes: " + ex.Message);
                bld.Append(Environment.NewLine);
            }
            return null;
        }

        /// <summary>
        /// Works only in single user environment
        /// </summary>
        /// <param name="appCmdLine"></param>
        /// <returns></returns>
        public static bool Launch_UseFirstExplorerToken(string appCmdLine)
        {
            Process[] ps = Process.GetProcessesByName("explorer");
            //log.Info("Detected " + ps.Length + " processes with name \"explorer\"");
            int processId = -1;//=processId
            if (ps.Length > 0)
            {
                processId = ps[0].Id;
            }
            return Launch(appCmdLine, processId);
        }

        public static bool Launch_UseProcessOfUser(string appCmdLine, string userName)
        {
            return Launch_UseProcessOfUser(appCmdLine, userName, "explorer");
        }

        public static bool Launch_UseProcessOfUser(string appCmdLine, string userName, string processName)
        {
            try
            {
                StringBuilder bld = null;
                Process p = GetProcessOfUser(userName, processName, ref bld);
                int processId = -1;//=processId
                if (p != null)
                {
                    processId = p.Id;
                    //log.Info("Process for user " + userName + " found (" + p.Id.ToString() + ")");
                    //log.Debug(bld.ToString());
                }
                else
                {
                    //log.Error("No process for user " + userName + " found");
                    //log.Debug(bld.ToString());
                }
                return Launch(appCmdLine, processId);
            }
            catch (Exception)
            {
                //log.Error("Exception in Launch_UseProcessOfUser: " + e.Message, e);
                return false;
            }
        }

        private static bool Launch(string appCmdLine, int processId)
        {

            bool ret = false;

            if (processId > 1)
            {
                IntPtr token = GetPrimaryToken(processId);

                if (token != IntPtr.Zero)
                {

                    IntPtr envBlock = GetEnvironmentBlock(token);
                    ret = LaunchProcessAsUser(appCmdLine, token, envBlock);
                    if (envBlock != IntPtr.Zero)
                    {
                        DestroyEnvironmentBlock(envBlock);
                    }

                    CloseHandle(token);
                }

            }
            return ret;
        }

    }

}


