﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml;
using log4net;
using System.Net.Http;
using System.Net.Http.Headers;
using Cassia;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Threading;
using System.Reflection;

namespace ASConnect
{
    public class Program
    {
        private static string _regKey = "SOFTWARE\\POS.AG\\signPOS Client";
        private static string _OrderNr = "";
        private static int _maxDocuments = 1;
        private static readonly ILog LOGGER = LogManager.GetLogger(typeof(Program));
        private static byte[] zipBytes = null;
        private static string GUID = "";

        private static string user = "";
        private static string password = "";
        private static string PLUGIN_NAME = "";
        private static string url = "";
        private static bool Send2ServerOk = true;

        private static string tempFolder = "";

        //private static string appGuid = "0d71a608-6098-474d-aca4-0eadbcd43230";

        //get current assembly directory
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);

            }
        }

        public static void Main(string[] args)
        {
            try
            {
                //check if there exist a temp folder
                tempFolder = System.Configuration.ConfigurationManager.AppSettings["TempPath"].ToString();
                if (String.IsNullOrEmpty(tempFolder)) tempFolder = @"c:\signPOS\temp";
                if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);
                //------------------------

                Thread pb = new Thread(startProgressBar);
                try
                {
                    pb.Start();
                }
                catch (Exception) { }

                Assembly assembly = Assembly.GetExecutingAssembly();
                //var attribute = (System.Runtime.InteropServices.GuidAttribute)assembly.GetCustomAttributes(typeof(System.Runtime.InteropServices.GuidAttribute), true)[0];
                //var id = attribute.Value;

                //using (Mutex mutex = new Mutex(false, id))
                //{
                //    if (!mutex.WaitOne(0, false))
                //    {
                //        return;
                //    }

                //    GC.Collect();
                //}

                System.Diagnostics.Process _currentProc = System.Diagnostics.Process.GetCurrentProcess();
                //Check process ASConnect
                System.Diagnostics.Process[] _processList = System.Diagnostics.Process.GetProcessesByName("ASConnect");
                foreach (System.Diagnostics.Process prc in _processList)
                {
                    try
                    {
                        if (prc.StartTime.Ticks < _currentProc.StartTime.Ticks)
                        {
                            System.Environment.Exit(-1);
                        }
                    }
                    catch (Exception) { }
                }
                Thread.Sleep(200);
                //check username for advanced logging
                //get current user
                string _userName = "";
                try
                {
                    ITerminalServicesManager _manager = new TerminalServicesManager();
                    using (ITerminalServer server = _manager.GetLocalServer())
                    {
                        server.Open();
                        _userName = _manager.CurrentSession.UserName;
                    }
                }
                catch (Exception) { }
                //init logging
                log4net.GlobalContext.Properties["LogName"] = _currentProc.Id + _userName + "_ASConnect_log.txt";
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(AssemblyDirectory + "\\ASConnect.xml"));
                LOGGER.Info("----- ASCONNECT START -----");
                System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
                string version = fvi.FileVersion;
                LOGGER.Info("ASConnect Version: " + version);

                if (args.Length > 0)
                {
                    if (File.Exists(args[0]))
                    {
                        try
                        {
                            XmlDocument xml = new XmlDocument();
                            xml.Load(args[0]);

                            _OrderNr = xml.SelectSingleNode("InteractiveDataModel/Publication/Order_Nr").InnerText;
                            _maxDocuments = Convert.ToInt32(xml.SelectSingleNode("InteractiveDataModel/Publication/MaxDocuments").InnerText);

                            try
                            {
                                //read server configuration
                                PLUGIN_NAME = xml.SelectSingleNode("InteractiveDataModel/Publication/SIGNPOSDLL").InnerText;
                                url = xml.SelectSingleNode("InteractiveDataModel/Publication/SIGNPOSSERVER").InnerText;
                                string loginData = xml.SelectSingleNode("InteractiveDataModel/Publication/SIGNPOSLOGIN").InnerText;
                                string userData = loginData.Substring(0, loginData.IndexOf(":"));
                                string passData = loginData.Substring(loginData.IndexOf(":") + 1, loginData.Length - (loginData.IndexOf(":") + 1));

                                bool result = Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult)
                                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

                                if (!result)
                                {
                                    System.Windows.Forms.MessageBox.Show("URL in XML not wellformed");
                                    url = "";
                                }

                                //decrypt login data
                                user = CryptPbkdf2(userData, "abc123", false);
                                password = CryptPbkdf2(passData, "abc123", false);

                            }
                            catch (Exception errorData) { LOGGER.Error("Error in reading server logindata from xml: " + errorData.ToString()); }


                            LOGGER.Info("OrderNr: " + _OrderNr + " MaxDocuments: " + _maxDocuments);
                            DirectoryInfo _DirInfo = new DirectoryInfo(Path.GetDirectoryName(args[0]));

                            FileInfo[] files = _DirInfo.GetFiles("*.e1d", SearchOption.AllDirectories);
                            List<string> documentList = new List<string>();
                            if (files.Length >= _maxDocuments)
                            {
                                MemoryStream ms = new MemoryStream();
                                ZipArchive zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true);

                                foreach (FileInfo fileInfo in files)
                                {
                                    try
                                    {
                                        XmlDocument xmlDoc = new XmlDocument();
                                        xmlDoc.Load(fileInfo.FullName);
                                        string OrderNr = xmlDoc.SelectSingleNode("InteractiveDataModel/Publication/Order_Nr").InnerText;
                                        string TemplateName = xmlDoc.SelectSingleNode("InteractiveDataModel/Publication/TEMPLATEID").InnerText;

                                        LOGGER.Info("OrderNr: " + OrderNr);
                                        LOGGER.Info("TemplateID: " + TemplateName);

                                        if (OrderNr.Equals(_OrderNr))
                                        {
                                            //check documentnr
                                            string _documentNr = xmlDoc.SelectSingleNode("InteractiveDataModel/Publication/DocumentNr").InnerText;
                                            int _docNr = 0;
                                            bool _parseDocumentNrOk = Int32.TryParse(_documentNr, out _docNr);
                                            if (_parseDocumentNrOk && _docNr <= _maxDocuments)
                                            {
                                                if (!documentList.Contains(_documentNr)) documentList.Add(_documentNr);
                                                {
                                                    string zipFilename = "temp" + _docNr + ".xml";
                                                    //zipArchive.CreateEntryFromFile(fileInfo.FullName, fileInfo.Name);
                                                    zipArchive.CreateEntryFromFile(fileInfo.FullName, zipFilename);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LOGGER.Error("File " + fileInfo.FullName + " was not ok");
                                        try
                                        {
                                            Path.ChangeExtension(fileInfo.FullName, ".error");
                                        }
                                        catch (Exception) { }
                                    }
                                }
                                if (documentList.Count == _maxDocuments)
                                {
                                    LOGGER.Info("Documentset for OrderNr " + _OrderNr + " is complete, preparing upload.");
                                    LOGGER.Info("----- Documents in zipArchive -----");
                                    //create object for upload to POSPortal Server
                                    zipArchive.Dispose();

                                    zipBytes = ms.ToArray();
                                    File.WriteAllBytes(tempFolder + "\\ZipArchiv.zip", zipBytes);

                                    Dictionary<string, string> xmlDocs = new Dictionary<string, string>();
                                    System.IO.Compression.ZipArchive zip = new System.IO.Compression.ZipArchive(ms, System.IO.Compression.ZipArchiveMode.Read, true);
                                    foreach (System.IO.Compression.ZipArchiveEntry zipFile in zip.Entries)
                                    {
                                        Stream mss = new MemoryStream();
                                        mss = zipFile.Open();
                                        string msString = new StreamReader(mss, System.Text.Encoding.GetEncoding("ISO-8859-1")).ReadToEnd();

                                        XmlDocument _xml = new XmlDocument();
                                        _xml.LoadXml(msString);
                                        string _documentNr = _xml.SelectSingleNode("InteractiveDataModel/Publication/DocumentNr").InnerText;
                                        xmlDocs.Add(_documentNr, msString);
                                    }
                                    zip.Dispose();

                                    ms.Dispose();
                                    SendAsync();
                                    if (Send2ServerOk)
                                    {
                                        //remove e1d files
                                        DirectoryInfo _DirectoryInfo = new DirectoryInfo(Path.GetDirectoryName(args[0]));
                                        LOGGER.Debug("Delete files in: " + _DirectoryInfo.FullName);
                                        FileInfo[] fileList = _DirectoryInfo.GetFiles("*.e1d", SearchOption.AllDirectories);
                                        LOGGER.Debug("Filecount in folder: " + fileList.Length);
                                        foreach (FileInfo fileInfo in fileList)
                                        {
                                            try
                                            {
                                                fileInfo.Delete();
                                                LOGGER.Debug("File: " + fileInfo.Name + " successfully deleted.");
                                            }
                                            catch (Exception errorDelete) { LOGGER.Error("Error in deleting e1d file: " + errorDelete.ToString()); }
                                        }
                                        StartClient();
                                        try
                                        {
                                            pb.Abort();
                                        }
                                        catch (Exception) { }
                                    }
                                    else
                                    {
                                        System.Windows.Forms.MessageBox.Show("Error in sending documentset to server.");
                                        LOGGER.Debug("");
                                    }
                                }
                                else
                                {
                                    //document set is not complete
                                    System.Windows.Forms.MessageBox.Show("Documentset not complete, please retry.");
                                    LOGGER.Info("Documentset for OrderNr " + _OrderNr + " is not complete.");
                                    return;
                                }
                            }
                            else
                            {
                                //document set is not complete
                                System.Windows.Forms.MessageBox.Show("Documentset not complete, please retry.");
                                LOGGER.Info("Documentset for OrderNr " + _OrderNr + " is not complete.");
                                return;
                            }
                        }
                        catch (Exception ex)
                        {
                            LOGGER.Error("Error ASConnect in xml: " + ex.ToString());
                            System.Windows.Forms.MessageBox.Show("Error in reading xml data, please retry.");
                            try
                            {
                                Path.ChangeExtension(args[0], ".error");
                            }
                            catch (Exception) { }
                        }
                    }

                }
                try
                {
                    pb.Abort();
                }
                catch (Exception) { }
                LOGGER.Info("----- ASCONNECT END ------");
            }
            catch (Exception) { }
        }

        private static void startProgressBar()
        {
            ProgressBar pb = new ProgressBar();

            pb.ShowDialog();

        }

        public static void StartClient()
        {
            LOGGER.Info("Start signPOS Client with GUID: " + GUID);

            string _targetApp = System.Configuration.ConfigurationManager.AppSettings["signPOSExePath"].ToString();
            if (String.IsNullOrEmpty(user)) user = System.Configuration.ConfigurationManager.AppSettings["SERVER_USERNAME"].ToString();
            if (String.IsNullOrEmpty(password)) password = System.Configuration.ConfigurationManager.AppSettings["SERVER_PASSWORD"].ToString();
            if (String.IsNullOrEmpty(PLUGIN_NAME)) PLUGIN_NAME = System.Configuration.ConfigurationManager.AppSettings["SERVER_PLUGIN_NAME"].ToString();
            if (String.IsNullOrEmpty(url)) url = System.Configuration.ConfigurationManager.AppSettings["SERVER_API_URL"].ToString();

            try
            {
                RegistryKey _installPath = Registry.LocalMachine.OpenSubKey(_regKey);
                if (_installPath != null)
                {
                    _targetApp = (string)_installPath.GetValue("InstallPath");
                }
                else
                {
                    //check win64
                    RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
                    _installPath = localKey.OpenSubKey(_regKey);
                    if (_installPath != null)
                    {
                        _targetApp = (string)_installPath.GetValue("InstallPath");
                    }
                }
            }
            catch (Exception) { }

            password = CryptPbkdf2(password, "abc123", true);

            string AppStart = "\"" + _targetApp + "\"" + " -signaturerequest \"" + GUID + "\"" + " -api_url \"" + url + "\"" + " -api_user \"" + user + "\"" + " -api_password \"" + password + "\"" + " -api_plugin \"" + PLUGIN_NAME + "\"";

            LOGGER.Debug("startClient - starting URL: " + AppStart);
            try
            {
                //ProcessAsUser.Launch_UseFirstExplorerToken(AppStart);

                System.Diagnostics.Process notePad = new System.Diagnostics.Process();

                notePad.StartInfo.FileName = _targetApp;
                notePad.StartInfo.Arguments = " -signaturerequest \"" + GUID + "\"" + " -api_url \"" + url + "\"" + " -api_user \"" + user + "\"" + " -api_password \"" + password + "\"" + " -api_plugin \"" + PLUGIN_NAME + "\"";

                notePad.Start();
            }
            catch (Exception) { }

        }

        private static void SendAsync()
        {
            LOGGER.Info("Uploading Documentset to server");
            using (var client = new HttpClient())
            {
                try
                {
                    if (String.IsNullOrEmpty(user)) user = System.Configuration.ConfigurationManager.AppSettings["SERVER_USERNAME"].ToString();
                    if (String.IsNullOrEmpty(password)) password = System.Configuration.ConfigurationManager.AppSettings["SERVER_PASSWORD"].ToString();
                    if (String.IsNullOrEmpty(PLUGIN_NAME)) PLUGIN_NAME = System.Configuration.ConfigurationManager.AppSettings["SERVER_PLUGIN_NAME"].ToString();
                    if (String.IsNullOrEmpty(url)) url = System.Configuration.ConfigurationManager.AppSettings["SERVER_API_URL"].ToString();

                    if (url.LastIndexOf("/") == url.Length -1) url = url.Substring(0, url.LastIndexOf("/"));

                    //client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", String.Format("Basic {0}", Convert.ToBase64String(Encoding.Default.GetBytes(user + ":" + password))));

                    ByteArrayContent byteContentFinal = new ByteArrayContent(zipBytes); // Document with signatures       
                    //HttpResponseMessage response = await client.PutAsync(String.Format("signflows/signaturerequests/{0}/signeddoc?plugInId={1}", Process._NextSignatureRequestId, PLUGIN_NAME), byteContentFinal);
                    var response = client.PostAsync(String.Format(url + "/actions?plugInId={0}", PLUGIN_NAME), byteContentFinal).Result;
                    //var response = client.PostAsync(String.Format("?plugInId={0}", PLUGIN_NAME), byteContentFinal).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //response OK
                        GUID = response.Content.ReadAsStringAsync().Result;
                        if (GUID.Contains("\"Data\":\""))
                        {
                            int idxFirst = GUID.IndexOf("\"Data\":\"") + "\"Data\":\"".Length;
                            string firstPart = GUID.Substring(idxFirst);
                            int idxSec = firstPart.IndexOf("\",\"");
                            GUID = firstPart.Substring(0, idxSec);
                        }
                    }
                    else
                    {
                        //response NOK
                        string nok = response.StatusCode.ToString();
                        LOGGER.Debug("Error in uploading documentset: " + response.ReasonPhrase);
                        Send2ServerOk = false;
                    }
                }
                catch (Exception ex) { LOGGER.Error("Error in SendAsync: " + ex.ToString()); LOGGER.Error("URL: " + url); }
            }
        }

        // PB-KDF2 AES encryption - used to encrypt config passwords
        // data=my message, password=my Passwd, encrypt=true to encrypt, false to decrypt
        private static string CryptPbkdf2(string data, string password, bool encrypt)
        {
            var salt = new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 };
            var passwordBytes = new Rfc2898DeriveBytes(password, salt);

            using (Aes engine = Aes.Create())
                return CryptSymmetrically(data, encrypt, passwordBytes, engine);
        }

        private static string CryptSymmetrically(string data, bool encrypt, DeriveBytes passwordBytes, SymmetricAlgorithm engine)
        {
            engine.Key = passwordBytes.GetBytes(32);
            engine.IV = passwordBytes.GetBytes(16);

            ICryptoTransform transform = (encrypt) ? engine.CreateEncryptor() : engine.CreateDecryptor();

            using (var outStream = new MemoryStream())
            using (var cryptoStream = new CryptoStream(outStream, transform, CryptoStreamMode.Write))
            {
                byte[] dataBytes = encrypt ? Encoding.Unicode.GetBytes(data) : Convert.FromBase64String(data);

                cryptoStream.Write(dataBytes, 0, dataBytes.Length);
                cryptoStream.Close();

                if (encrypt)
                    return Convert.ToBase64String(outStream.ToArray());

                return Encoding.Unicode.GetString(outStream.ToArray());
            }
        }

    }
}
